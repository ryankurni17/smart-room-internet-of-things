#include "ACS712.h"
#include <ArduinoJson.h>
#include <SoftwareSerial.h>
SoftwareSerial nodemcu(5, 6);

ACS712  ACS1(A0, 5.0, 1023, 185);
ACS712  ACS2(A1, 5.0, 1023, 185);
ACS712  ACS3(A2, 5.0, 1023, 185);

float ReadACS1, ReadACS2, ReadACS3;

void setup() {  
  Serial.begin(9600);
  nodemcu.begin(9600);
  Serial.println(__FILE__);

  ACS1.autoMidPoint();
  Serial.print("MidPoint: ");
  Serial.print(ACS1.getMidPoint());
  Serial.print(". Noise mV: ");
  Serial.println(ACS1.getNoisemV());

  ACS2.autoMidPoint();
  Serial.print("MidPoint: ");
  Serial.print(ACS2.getMidPoint());
  Serial.print(". Noise mV: ");
  Serial.println(ACS2.getNoisemV());

  ACS3.autoMidPoint();
  Serial.print("MidPoint: ");
  Serial.print(ACS3.getMidPoint());
  Serial.print(". Noise mV: ");
  Serial.println(ACS3.getNoisemV());
}

void loop() {
  StaticJsonDocument<1000> doc;

  ReadACS1 = ACS1.getMidPoint();
  float mA1 = ACS1.mA_AC();
  float Amp1 = (mA1/1000);
  float Wattage1 = (220*Amp1)-18;

  if(Wattage1 < 0.00){
    Wattage1 = 0;
  }

  Serial.print("Read Sensor ACS1: ");
  Serial.print(ReadACS1);
  Serial.print(" Ampere1: ");
  Serial.print(Amp1);
  Serial.print(" Watt1: ");
  Serial.print(Wattage1);
  Serial.print(". Form factor: ");
  Serial.println(ACS1.getFormFactor());

  ReadACS2 = ACS2.getMidPoint();
  float mA2 = ACS2.mA_AC();
  float Amp2 = (mA2/1000);
  float Wattage2 = (220*Amp2)-18;

  if(Wattage2 < 0.00){
    Wattage2 = 0;
  }

  Serial.print("Read Sensor ACS2: ");
  Serial.print(ReadACS2);
  Serial.print(" Ampere2: ");
  Serial.print(Amp2);
  Serial.print(" Watt2: ");
  Serial.print(Wattage2);
  Serial.print(". Form factor: ");
  Serial.println(ACS2.getFormFactor());

  ReadACS3 = ACS3.getMidPoint();
  float mA3 = ACS3.mA_AC();
  float Amp3 = (mA3/1000);
  float Wattage3 = (220*Amp3)-18;

  if(Wattage3 < 0.00){
    Wattage3 = 0;
  }

  Serial.print("Read Sensor ACS3: ");
  Serial.print(ReadACS3);
  Serial.print(" Ampere3: ");
  Serial.print(Amp3);
  Serial.print(" Watt3: ");
  Serial.print(Wattage3);
  Serial.print(". Form factor: ");
  Serial.println(ACS3.getFormFactor());
  Serial.println(" ");
  
  doc["AmpereSensor1"] = Amp1;
  doc["WattSensor1"] = Wattage1;
  doc["AmpereSensor2"] = Amp2;
  doc["WattSensor2"] = Wattage2;
  doc["AmpereSensor3"] = Amp3;
  doc["WattSensor3"] = Wattage3;
  
  serializeJson(doc, nodemcu);
  delay(500);
}
