#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "TMPLtv-7RxFm"
#define BLYNK_DEVICE_NAME "Smart Room IoT"
#define BLYNK_AUTH_TOKEN "muuQoXPUJJgqq__idAPdN5tw-FR3gaAv"

#include <SoftwareSerial.h>
#include <ArduinoJson.h>
//D6 = Rx & D5 = Tx
SoftwareSerial nodemcu(D6, D5); 
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] =  BLYNK_AUTH_TOKEN;

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "";  //  your name ssid
char pass[] = "";  //  your password

const int relay1 = D1;
const int relay2 = D2;
const int relay3 = D3;
const int MOTION_PIN1 = D0;
const int MOTION_PIN2 = D7;
const int MOTION_PIN3 = D8;

int proximity1, proximity2, proximity3;

BLYNK_WRITE(V0){
  digitalWrite(relay1, param.asInt());
}

BLYNK_WRITE(V1){
  digitalWrite(relay2, param.asInt());
}

BLYNK_WRITE(V2){
  digitalWrite(relay3, param.asInt());
}

WidgetLCD lcd(V9);

void setup() {
  // Initialize Serial port
  Serial.begin(9600);
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(relay3, OUTPUT);
  
  pinMode(MOTION_PIN1, INPUT);
  pinMode(MOTION_PIN2, INPUT);
  pinMode(MOTION_PIN3, INPUT);

  digitalWrite(relay1, HIGH);
  digitalWrite(relay2, HIGH);
  digitalWrite(relay3, HIGH);

  Blynk.begin(auth, ssid, pass);
  nodemcu.begin(9600);
  while (!Serial) continue;
}

void loop() {
  lcd.clear();
  lcd.print(0, 0, "'Smart Home IoT'");
  lcd.print(0, 1, "'Current Sensor'");

  Proximity();
  
  StaticJsonDocument<1000> doc;
  DeserializationError error = deserializeJson(doc, nodemcu);

  // Test parsing
  while (error) {
    Serial.println("Invalid JSON Object");
    delay(500);
    DeserializationError error = deserializeJson(doc, nodemcu);
  }

  Serial.println("Data Recieved");
  Serial.print("Ampere sensor 1:  ");
  float ampere1 = doc["AmpereSensor1"];
  Serial.println(ampere1);
  Serial.print("Watt sensor 1:  ");
  float watt1 = doc["WattSensor1"];
  Serial.println(watt1);
  Blynk.virtualWrite(V3, ampere1);
  Blynk.virtualWrite(V4, watt1);
  
  Serial.print("Ampere Sensor 2:  ");
  float ampere2 = doc["AmpereSensor2"];
  Serial.println( ampere2);
  Serial.print("Watt sensor 2:  ");
  float watt2 = doc["WattSensor2"];
  Serial.println(watt2);
  Blynk.virtualWrite(V5, ampere2);
  Blynk.virtualWrite(V6, watt2);

  Serial.print("Ampere Sensor 3:  ");
  float ampere3 = doc["AmpereSensor3"];
  Serial.println( ampere3);
  Serial.print("Watt sensor 3:  ");
  float watt3 = doc["WattSensor3"];
  Serial.println(watt3);
  Blynk.virtualWrite(V7, ampere3);
  Blynk.virtualWrite(V8, watt3);

  Serial.println("-----------------------------------------");
  Blynk.run();
}

void Proximity(){
  
  proximity1 = digitalRead(MOTION_PIN1);
    if (proximity1 == HIGH)
    {
      digitalWrite(relay1,LOW);
    }

  proximity2 = digitalRead(MOTION_PIN2);
    if (proximity2 == HIGH)
    {
      digitalWrite(relay2,LOW);
    }

  proximity3 = digitalRead(MOTION_PIN3);
    if (proximity3 == HIGH)
    {
      digitalWrite(relay3,LOW);
    }  
}
